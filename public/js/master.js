(function() {
  var componentInject = ['ngRoute', 'fareEstimator', 'previousOrders', 'main', 'merchantProfile', 'customerAddressBook', 'initiateRequest','updateProfile'];
  var dashboard = angular.module('merchantDashboard', componentInject);

  dashboard.config(function($routeProvider) {

    $routeProvider
      .when("/", {
        template: "<main></main>"
      })
      .when("/home", {
        redirectTo: "/"
      })
      .when("/orderHistory", {
        template: '<previous-orders></previous-orders>'
      })
      .when("/myCustomers", {
        template: '<customer-address-book></customer-address-book>'
      })
      .when('/account', {
        template: '<merchant-profile></merchant-profile>'
      })
      .when('/quickQuotation', {
        template: '<fare-estimator></fare-estimator>'
      })
      .when('/completeProfile', {
        template: '<update-profile></update-profile>'
      })
      .otherwise({
        template: "<div class='ui icon error message'><i class='warning icon'></i><div class='content'><div class='header'>Something really went wrong</div><p>Our engineers are working on it.</p></div></div>"
      })
  });

})();
