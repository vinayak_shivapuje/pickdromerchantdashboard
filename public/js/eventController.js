(function() {
  $(document).ready(function() {
    $('#loading').hide();
    $('.ui.dropdown').dropdown();
    $('.vertical.menu a.item').click(function() {
      $('.vertical.menu a.item').removeClass("active");
      $(this).addClass("active");
    });
  });
})();
