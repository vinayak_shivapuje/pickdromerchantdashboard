angular.module('previousOrders', []);
angular.module('previousOrders').component('previousOrders', {
  templateUrl: 'components/previousOrders/previousOrders.template.html',
  controller: function($scope) {

    $scope.orders = [];
    var merchantId = localStorage.getItem('firebase:uid');
    var clientOrdersRef = firebase.database().ref('clientOrders/' + merchantId);

    var db = firebase.database();

    clientOrdersRef.on("value", function(dataSnap) {
      dataSnap.forEach(function(childSnapshot) {
        if (childSnapshot.val().status === 'initiated') {
          db.ref('orders/incoming/').child(childSnapshot.key).once("value", function(snap) {
            updateView(childSnapshot.key, snap.val());
          });
        } else if (childSnapshot.val().status === 'progress') {
          db.ref('orders/progress/').child(childSnapshot.key).once("value", function(snap) {
            updateView(childSnapshot.key, snap.val());
          });
        } else {
          db.ref('orders/completed/').child(childSnapshot.key).once("value", function(snap) {
            updateView(childSnapshot.key, snap.val());
          });
        }
      });
    });

    function updateView(key, requestDetails) {

      var getName = function() {
        if (requestDetails.requestType == "pickup") {
          return requestDetails.pickup.name;
        } else {
          return requestDetails.drop.name;
        }
      }

      var getContactNumber = function() {
        if (requestDetails.requestType == "pickup") {
          return requestDetails.pickup.contactNumber;
        } else {
          return requestDetails.drop.contactNumber;
        }
      }
      var getLocation = function() {
        if (requestDetails.requestType == "pickup") {
          return requestDetails.pickup.address;
        } else {
          return requestDetails.drop.address;
        }
      }

      $scope.orders.push({
        orderId: key,
        requestDate: requestDetails.date,
        location: getLocation(),
        requestTime: requestDetails.time,
        serviceType: requestDetails.requestType,
        status: requestDetails.status,
        deliveryCharge: requestDetails.serviceCharge,
        customerName: getName(),
        customerContactNumber: getContactNumber()
      });
      $scope.$apply();
    }
    console.log($scope.orders);
    $scope.getClass = function(status) {
      if (status == "initiated") {
        return "negative";
      } else if (status == "progress") {
        return "warning";
      } else if (status == "completed") {
        return "positive";
      }
    }
  }
});
