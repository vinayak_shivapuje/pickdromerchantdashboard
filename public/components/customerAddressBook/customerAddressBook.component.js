var customerAddressBookApp = angular.module('customerAddressBook', ['firebase']);

angular.module('customerAddressBook').component('customerAddressBook', {
  templateUrl: 'components/customerAddressBook/customerAddressBook.template.html',
  controller: function($scope, $firebaseArray) {

    var uid = localStorage.getItem('firebase:uid');
    var customerRef = firebase.database().ref('/merchantClients/').child(uid);
    $scope.customers = $firebaseArray(customerRef);

    $('.ui.segment.attached.loading').removeClass('loading');

    //add New customer database
    $scope.addCustomer = function() {
      customerRef.push($scope.customer);
    }

    //edit the existed customer details
    $scope.editExistingCustomer = function(customer) {
      angular.element('#editCustomerWindow').modal('show');
      $scope.editCustomer = customer;
    }

    $scope.update = function() {
      $scope.customers.$save($scope.editCustomer).then(function() {
        $('#editCustomerWindow').modal('hide');
      });
    }
    //delete the customer
    $scope.deleteCustomer = function(customer) {
      var response = confirm('Are you sure you want to remove ' + customer.recieverName + ' ?');
      if (response) {
        $scope.customers.$remove(customer);
      }

    }

    $scope.getClass = function(value) {
      if (value == 'male') {
        return 'blue male';
      } else if (value == 'female') {
        return 'pink female';
      } else {
        return 'green genderless'
      }
    }
  }
});
