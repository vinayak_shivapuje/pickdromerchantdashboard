angular.module('main', ['firebase']);
angular.module('main').component('main', {
  templateUrl: 'components/main/main.template.html',
  controller: function($scope) {
    var merchantId = localStorage.getItem('firebase:uid');
    var clientOrdersRef = firebase.database().ref('clientOrders/' + merchantId);

    var db = firebase.database();

    $scope.ongoingCount = 0;
    $scope.completedCount = 0;
    $scope.orders = [];
    clientOrdersRef.on("value", function(dataSnap) {
      dataSnap.forEach(function(childSnapshot) {
        if (childSnapshot.val().status === 'initiated') {
          db.ref('orders/incoming/').child(childSnapshot.key).once("value", function(snap) {
            $scope.ongoingCount++;
            updateView(childSnapshot.key, snap.val());
          });
        } else if (childSnapshot.val().status === 'progress') {
          db.ref('orders/progress/').child(childSnapshot.key).once("value", function(snap) {
            $scope.ongoingCount++;
            updateView(childSnapshot.key, snap.val());
          });
        } else {
          db.ref('orders/completed/').child(childSnapshot.key).once("value", function(snap) {
            $scope.completedCount++;
            updateView(childSnapshot.key, snap.val());
          });
        }
      });

    });

    function updateView(key, requestDetails) {

      var getName = function() {
        if (requestDetails.requestType == "pickup") {
          return requestDetails.pickup.name;
        } else {
          return requestDetails.drop.name;
        }
      }

      var getContactNumber = function() {
        if (requestDetails.requestType == "pickup") {
          return requestDetails.pickup.contactNumber;
        } else {
          return requestDetails.drop.contactNumber;
        }
      }
      var getLocation = function() {
        if (requestDetails.requestType == "pickup") {
          return requestDetails.pickup.address;
        } else {
          return requestDetails.drop.address;
        }
      }

      $scope.orders.push({
        orderId: key,
        date: requestDetails.date,
        location: getLocation(),
        time: requestDetails.time,
        serviceType: requestDetails.requestType,
        status: requestDetails.status,
        deliveryCharge: requestDetails.serviceCharge,
        customerName: getName(),
        customerContactNumber: getContactNumber()
      });
      $scope.$apply();
    }
    // console.log($scope.requests);

    $scope.isRequestsNotEmpty = function() {
      if ($scope.requests[0] === 'undefined') {
        return false;
      } else {
        return true;
      }
    }
  }
});
