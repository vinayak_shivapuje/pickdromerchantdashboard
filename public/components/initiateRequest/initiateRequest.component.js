angular.module('initiateRequest', ['firebase']);

angular.module('initiateRequest').component('initiateRequest', {
  templateUrl: 'components/initiateRequest/initiateRequest.template.html',
  controller: function($scope) {
    var db = firebase.database();
    var rootRef = db.ref();
    var incomingOrdersRef = db.ref('/orders/');
    var uid = localStorage.getItem('firebase:uid');
    // var ordersRef = db.ref('/orders/');

    var merchantDetails = {
      businessName: 'Curlies Baking',
      contactName: 'Geetha P',
      contactNumber: '8899543675',
      address: '#378, ISRO layout, Bangalore',
      geo: {
        lat: 4.66,
        lang: 1.32
      }
    }


    var today = new Date();
    var currentDate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var currentTime = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    //generate drop request
    $scope.dropRequest = function(dropValues) {
      var order = {
        pickup: {
          address: merchantDetails.address,
          contactNumber: merchantDetails.contactNumber,
          name: merchantDetails.contactName
        },
        drop: {
          address: dropValues.recieverAddress,
          contactNumber: dropValues.recieverContactNumber,
          name: dropValues.recieverName
        },
        packageDetails: {
          vehicleRequired: dropValues.vehicleType,
          // packageWeight: dropValues.packageWeight,
          instruction: dropValues.specialInstruction
        },
        date: currentDate,
        time: currentTime,
        requestType: 'drop',
        distance: null,
        serviceCharge: null,
        clientId: uid,
        status: {
          accepted: false,
          cashCollected: false,
          dropped: false,
          pickedUp: false
        }
      }
      generateRequest(order);
    }

    //generate pickUp request
    $scope.pickupRequest = function(pickupValues) {
      var order = {
        pickup: {
          address: pickupValues.senderAddress,
          contactNumber: pickupValues.senderContactNumber,
          name: pickupValues.senderName
        },
        drop: {
          address: merchantDetails.address,
          contactNumber: merchantDetails.contactNumber,
          name: merchantDetails.contactName
        },
        packageDetails: {
          vehicleRequired: pickupValues.vehicleType,
          // packageWeight: pickupValues.packageWeight,
          instruction: pickupValues.specialInstruction
        },
        date: currentDate,
        time: currentTime,
        requestType: 'pickup',
        distance: null,
        serviceCharge: null,
        clientId: uid,
        status: {
          accepted: false,
          cashCollected: false,
          dropped: false,
          pickedUp: false
        }
      }
      generateRequest(order);
    }

    function generateRequest(orderData) {
      //In future we'll replace this id with our own customized orderId
      var orderId = incomingOrdersRef.child('incoming').push().key;

      var updateOrder = {};
      updateOrder['/orders/incoming/' + orderId] = orderData;
      updateOrder['/clientOrders/' + uid + '/' + orderId] = {
        status: 'initiated'
      };
      rootRef.update(updateOrder)
        .then(function(success) {
          //window.location = '/';
          $('initiate-request').transition({
            animation: 'slide up',
            onComplete: function() {
              $('#requestService').transition({
                animation: 'slide down'
              });
            }
          });
          $('#requestSuccess').removeClass('hidden');
          $('#requestSuccess').addClass('violet');
        }, function(error) {
          console.log(error);
        });
    }
  }
});
