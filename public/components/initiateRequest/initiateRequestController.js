$(document).ready(function() {

  //Pickup and drop chooser section start
  $('#pickUpDetailsSection').hide();
  $('#pickupPackage').on('click', function() {
    $('#pickUpDetailsSection').show();
    $('#dropDetailsSection').hide();
    $(this).addClass('active');
    $('#dropPackage').removeClass('active');
  });

  $('#dropPackage').on('click', function() {
    $('#dropDetailsSection').show();
    $('#pickUpDetailsSection').hide();
    $(this).addClass('active');
    $('#pickupPackage').removeClass('active');
  });
  //Pickup and drop chooser section end

  // $('#modifyRequest').on('click', function() {
  //   $("#summaryOfRequest").transition({
  //     animation: 'fly left',
  //     onComplete: function() {
  //       $("#initiateRequest").transition({
  //         animation: 'fly right'
  //       });
  //     }
  //   });
  // });

  $('.ui.dropdown').dropdown();

  //closing message
  $('.message .close')
    .on('click', function() {
      $(this)
        .closest('.message')
        .transition('fade');
    });

  // Validating the input fields drop section
  $('#dropDetailsSection').form({
    fields: {
      recieverName: ['empty', 'regExp[/^(\s)*[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])+))*(\s)*$/]'],
      recieverContactNumber: ['exactLength[10]', 'empty', 'number'], // 'regExp[/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/]'],
      // specialInstruction: 'empty',
      vehicleType: 'empty',
      recieverAddress: 'empty',
    },
    onSuccess: function() {
      $('#dropDetailsSection').addClass('loading');
      var dropDetails = $('#dropDetailsSection').form('get values');
      angular.element('#initiateRequest').scope().dropRequest(dropDetails);
      $('#dropDetailsSection').form('clear');
      $('#dropDetailsSection').removeClass('loading');
    }
  });

  // Validating the input fields of pickup section
  $('#pickUpDetailsSection').form({
    fields: {
      senderName: ['empty', 'regExp[/^(\s)*[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])+))*(\s)*$/]'],
      senderContactNumber: ['exactLength[10]', 'empty', 'number'], // 'regExp[/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/]'],
      // specialInstruction: 'empty',
      vehicleType: 'empty',
      senderAddress: 'empty',
    },
    onSuccess: function() {
      $('#pickUpDetailsSection').addClass('loading');
      var pickupDetails = $('#pickUpDetailsSection').form('get values');
      angular.element('#initiateRequest').scope().pickupRequest(pickupDetails);
      $('#pickUpDetailsSection').form('clear');
      $('#pickUpDetailsSection').removeClass('loading');
    }
  });

  $('#cancel').on('click', function() {
    $('initiate-request').transition({
      animation: 'drop',
      onComplete: function() {
        $('#requestService').transition({
          animation: 'drop'
        });
      }
    });
  });
});
