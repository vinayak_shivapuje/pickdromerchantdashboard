angular.module('fareEstimator', []);
angular.module('fareEstimator').component('fareEstimator', {
  templateUrl: "components/fareEstimator/fareEstimator.template.html",
  controller: function($scope) {
    var source, destination;
    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();

    new google.maps.places.Autocomplete(document.getElementById('pickupLocation'));
    new google.maps.places.Autocomplete(document.getElementById('dropLocation'));
    directionsDisplay = new google.maps.DirectionsRenderer({
      'draggable': true
    });

    $scope.GetRoute = function() {
      map = new google.maps.Map(document.getElementById('costEstimatorMap'));
      source = document.getElementById("pickupLocation").value;
      destination = document.getElementById("dropLocation").value;

      directionsDisplay.setMap(map);

      // *********DIRECTIONS AND ROUTE**********************//
      var request = {
        origin: source,
        destination: destination,
        travelMode: google.maps.TravelMode.DRIVING
      };
      directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(response);
        }
      });

      //*********DISTANCE AND DURATION**********************//
      var service = new google.maps.DistanceMatrixService();
      service.getDistanceMatrix({
        origins: [source],
        destinations: [destination],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false
      }, function(response, status) {
        if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
          var distance = response.rows[0].elements[0].distance.text;
          var duration = response.rows[0].elements[0].duration.text;
          var dvDistance = document.getElementById("dvDistance");
          calculatePrice(parseFloat(distance.replace(/[^\d\.]*/g, '')));
          dvDistance.innerHTML += "Duration:" + duration;
        } else {
          alert("Unable to find the distance via road.");
        }
      });
    }

    function calculatePrice(distance) {
      console.log(distance);
    }
  }
});
