angular.module('updateProfile', []);

angular.module('updateProfile').component('updateProfile', {
  templateUrl: "components/updateProfile/updateProfile.template.html",
  controller: function($scope) {
    var rootRef = firebase.database().ref();
    var merchantId = localStorage['firebase:uid'];
    $scope.updateProfile = function(values) {
      values['profileStatus'] = "completed";
      values['userType'] = "merchant";
      var businessDetails = {};
      console.log(values);
      businessDetails['/users/' + merchantId] = values;
      rootRef.update(businessDetails).then(function(success) {
        $('#updateProfile').removeClass('loading');
        $('#updateProfile').form('clear');
      }, function(error) {
        console.log(error);
      });
    }
  }
});
